# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=tetragon-client
pkgver=0.8.4
pkgrel=0
pkgdesc="CLI client for eBPF-based Security Observability and Runtime Enforcement"
url="https://github.com/cilium/tetragon"
arch="x86_64 aarch64" # fails to build on other platforms
license="Apache-2.0"
makedepends="go"
checkdepends="linux-headers"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/cilium/tetragon/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/tetragon-$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-ldflags "-X github.com/cilium/tetragon/pkg/version.Version=v$pkgver" \
		./cmd/tetra

	for shell in bash fish zsh; do
		./tetra completion $shell > tetra.$shell
	done
}

check() {
	# Some tests access example files inside the repo, so no -trimpath.
	# /tests/e2e: only run unittest
	# /pkg/reader/proc: read process name at PID 1
	# /pkg/selectors: read values in kernel Pid namespace
	# /pkg/alignchecker: fails to build bpf/objs/bpf_alignchecker.o
	# /pkg/sensors/{tracing,exec}, /pkg/{tracepoint,btf,bugtool}: try to access /sys/kernel
	# /pkg/sensors/exec/procevents, /pkg/cgroups: fail on CI (lacking cgroup controller "pids")
	GOFLAGS="${GOFLAGS/-trimpath/}" go test $(go list ./... | grep -v \
		-e '/pkg/alignchecker$' \
		-e '/pkg/btf$' \
		-e '/pkg/bugtool$' \
		-e '/pkg/cgroups$' \
		-e '/pkg/reader/proc$' \
		-e '/pkg/selectors$' \
		-e '/pkg/sensors/exec$' \
		-e '/pkg/sensors/exec/procevents$' \
		-e '/pkg/sensors/tracing$' \
		-e '/pkg/tracepoint$' \
		-e '/tests/e2e' \
		)
}

package() {
	install -Dm755 tetra -t "$pkgdir"/usr/bin/

	install -Dm644 tetra.bash \
		"$pkgdir"/usr/share/bash-completion/completions/tetra
	install -Dm644 tetra.fish \
		"$pkgdir"/usr/share/fish/completions/tetra.fish
	install -Dm644 tetra.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_tetra
}

sha512sums="
dad377b448efb269e29372455e97be8010a9ca4ce487240a0056cb6c0a8308da308304b4ed0f9ca6acf61e35a4145b248e94b38381a0c81a9c3a3e50d63d3fa1  tetragon-client-0.8.4.tar.gz
"
