# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=vector
pkgver=0.28.1
pkgrel=0
pkgdesc="High-performance observability data pipeline"
url="https://vector.dev/"
# s390x, ppc64le, riscv64: blocked by ring crate
# 32-bit: memory allocation error
arch="all !s390x !ppc64le !riscv64 !x86 !armhf !armv7"
license="MPL-2.0"
makedepends="
	cargo
	cmake
	librdkafka-dev
	openssl-dev
	perl
	protobuf-dev
	protoc
	python3
	"
checkdepends="bash cargo-nextest tzdata"
subpackages="$pkgname-doc $pkgname-openrc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/vectordotdev/vector/archive/refs/tags/v$pkgver.tar.gz
	skip-pkcs12-test.patch
	vector.initd
	vector.confd
	logrotate
	"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"
export OPENSSL_NO_VENDOR=1

prepare() {
	default_prepare

	# "cargo build" still tries to pull dependencies for other platforms
	cargo fetch --locked
}

build() {
	cargo build \
		--release \
		--frozen \
		--no-default-features \
		--features default-musl,component-validation-runner

	# cargo-nextest drops the release build, so back it up here
	cp -r target/release releasebuild
}

check() {
	# Upstream recommends running tests with "cargo nextest" instead of "cargo test"
	cargo nextest run \
		--fail-fast \
		--frozen \
		--no-default-features \
		--offline \
		--release \
		--workspace \
		--test-threads num-cpus
}

package() {
	install -Dm755 releasebuild/vector -t "$pkgdir"/usr/bin/
	install -Dm644 config/vector.toml -t "$pkgdir"/etc/vector/
	install -Dm644 "$srcdir"/logrotate "$pkgdir"/etc/logrotate.d/vector

	mkdir -p "$pkgdir"/usr/share/doc/vector
	cp -r config/examples "$pkgdir"/usr/share/doc/vector/examples

	install -Dm755 "$srcdir"/vector.initd "$pkgdir"/etc/init.d/vector
	install -Dm644 "$srcdir"/vector.confd "$pkgdir"/etc/conf.d/vector
}

sha512sums="
c8d31d8bfcf2316fdedbcb3c779cac371bc062ee833f0aeb9fb32b167b04a8ab9fe787e3666270badefe5a652d02176a8e019b9c63e770d0f880a9493f63c2ae  vector-0.28.1.tar.gz
7c7383005235eaba9bc050273d885b4348c0a3246abfc26da13acef495961e5930f1e309d9bb715216d14a35141a86de719804001cd5d10d33079384a06af5c5  skip-pkcs12-test.patch
806c2594d9d7b4bf1c24436a3982801a37ec3d8784acb97266eb7111fe35d8d05a64ef981100bd8aa35a71ad9c7c98de634428f696bded31993143ca572b6757  vector.initd
313f79e65e61754e8a611f8221d7c0cf36ee5af6f30aeff720924e64bb03d7f44c54fc31ae20926c354905f61df347830a7cba0c37afd41c1f59a25c52fa6f06  vector.confd
62db792de321655558bdb23ab9b3a7b35b84de445657011d88e8205cce4a926ff7b20f5304ec48fa646f1f259ad2136eceb5a377c4520071799da502eeff7592  logrotate
"
