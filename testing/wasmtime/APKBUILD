# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=wasmtime
pkgver=7.0.0
pkgrel=0
pkgdesc="Fast and secure runtime for WebAssembly"
url="https://wasmtime.dev/"
# others unsupported
arch="aarch64 x86_64"
license="Apache-2.0"
depends_dev="libwasmtime=$pkgver-r$pkgrel"
makedepends="cargo rust-wasm zstd-dev"
subpackages="libwasmtime $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/bytecodealliance/wasmtime/releases/download/v$pkgver/wasmtime-dev-src.tar.gz
	system-zstd.patch
	"
builddir="$srcdir/wasmtime-dev-src"
# net: fetch dependencies
# check: custom_limiter_detect_os_oom_failure fails with oom for some reason
options="!check net"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	git init
	# can't patch deps with vendor dir
	rm -fv .cargo/config.toml
	rm -rf vendor

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release
	cargo build --frozen --release --manifest-path crates/c-api/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/wasmtime -t "$pkgdir"/usr/bin/
	install -Dm644 target/release/libwasmtime.so -t "$pkgdir"/usr/lib/
	cp -a crates/c-api/include "$pkgdir"/usr/
}

libwasmtime() {
	amove usr/lib
}

sha512sums="
b2d5d17b397da38b2bd65319356347aed63a246dd52dbfa1fc775be67988329d6b9f2fa5714b1531901cc2c448dc5fd87eb098cc96bb23f643ed04f17c07f535  wasmtime-7.0.0.tar.gz
70307543f2ae4212f8629d3c4c05f60a46e2f7c83bcc52344a471afab1a345c7120a8c2edeeba648943add7d903a056a529201014d98a2b3087af1a1167b7134  system-zstd.patch
"
