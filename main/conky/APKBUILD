# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=conky
pkgver=1.19.0
pkgrel=0
pkgdesc="Advanced, highly configurable system monitor for X based on torsmo"
url="https://github.com/brndnmtthws/conky"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cairo-dev
	cmake
	curl-dev
	gawk
	glib-dev
	imlib2-dev
	libxdamage-dev
	libxext-dev
	libxft-dev
	libxinerama-dev
	libxml2-dev
	linux-headers
	lua5.4-dev
	ncurses-dev
	pango-dev
	samurai
	tolua++
	wayland-dev
	wayland-protocols
	wireless-tools-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/brndnmtthws/conky/archive/v$pkgver.tar.gz"

# temp allow textrels on riscv64
[ "$CARCH" = "riscv64" ] && options="$options textrels"

build() {
	cmake -B build -G Ninja \
		-DRELEASE=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_CURL=ON \
		-DBUILD_XDBE=ON \
		-DBUILD_IMLIB2=ON \
		-DBUILD_RSS=ON \
		-DBUILD_WLAN=ON \
		-DBUILD_I18N=OFF \
		-DBUILD_LUA_CAIRO=ON \
		-DBUILD_WAYLAND=ON \
		-DLUA_LIBRARIES="/usr/lib/lua5.4/liblua.so"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -D -m644 COPYING $pkgdir/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
6be9ce459cba3f35e6c6ad537afa9fffd9af926d4e190629bcdef1931622aad2220fa75c0ee8640066a101dae492a676e59bac85f21ab587349a6231ebe7e5ac  conky-1.19.0.tar.gz
"
