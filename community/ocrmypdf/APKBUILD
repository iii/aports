# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=14.0.4
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
# s390x, riscv64: pngquant
# armhf, armv7, x86, ppc64le: tesseract-ocr
arch="noarch !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
depends="
	ghostscript
	jbig2enc
	leptonica
	pngquant
	py3-coloredlogs
	py3-deprecation
	py3-img2pdf
	py3-packaging
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-pluggy
	py3-reportlab
	py3-tqdm
	python3
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
checkdepends="tesseract-ocr-data-eng py3-pytest py3-pytest-cov py3-pytest-xdist"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH=src \
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/ocrmypdf*.whl
}

sha512sums="
6c2f31ddb34ccd0a2ecf6fd8fa4fd619e3ad7a4e9dfb76487ad9b2aff605c43777046828f2c3fc777a545e6a2e2eb71c76e3b461d41f5ce7bbb43e7acc0c38f3  ocrmypdf-14.0.4.tar.gz
"
