# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=22.0.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
06352ae98fd997e77f7de182f49e274ef83e7df5ad12d39a623cfe96b242dfbccc56ec12dc611669db6727800ceb5bdec1d9e1d3907c26f5fa8b975416aec6a2  super-ttc-iosevka-22.0.0.zip
d6dc3c29a2e62ef967f55101bed3fbde13ac67bc0a316f25b57231ce9412693bd0a35363bd79f39909056cbcff0f050e496cda0ab15a46e85b67eb7919c1d21b  super-ttc-iosevka-aile-22.0.0.zip
84ee498853dce9aefa6c56a77ec67d3e724846650cfe50858d656e4344f1112679731f3e20147fde684e729fb8c3d8fb390a3f140b9118f7b4d035458b78508d  super-ttc-iosevka-slab-22.0.0.zip
a5a3dfd07b9c7667afb9a91a71a9d11c3b71ca1406cb56f7975bed855b293d17c8a7b230a9ea769d721605449c995a83548f1b24f7e28e8cb3411b15937b00fc  super-ttc-iosevka-curly-22.0.0.zip
5aa00a951f37bca8d311b765c0efb78d604227c0fbffbefdd0c3139a8ee46a0d9b3f0d3b798eacca1456de46381ad1583b6cff1574cf3650ff51ca7d163317f5  super-ttc-iosevka-curly-slab-22.0.0.zip
"
